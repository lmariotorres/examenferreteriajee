/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenferreteriajee.on;

import ec.edu.ups.examenferreteriajee.dao.ProductoDAO;
import ec.edu.ups.examenferreteriajee.dao.ProveedorDAO;
import ec.edu.ups.examenferreteriajee.modelo.Producto;
import ec.edu.ups.examenferreteriajee.modelos.Proveedor;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Luis Mario
 */

@Stateless
public class ObjetoNegocios implements ObjetoNegociosLocal{
    
    @Inject
   private ProveedorDAO pdao;
    
    @Inject
    private ProductoDAO prdao;
    
    
    public void insertarP(Proveedor proveedor, Producto producto){
        
        producto.setProveedor(proveedor);
      
        prdao.insertarProducto(producto);
    }
    
    
    public void solicitarP (int cantidad, Producto producto) throws Exception{
        
      
        producto.setStock(producto.getStock()+cantidad);
         prdao.actualizar(producto);
          
      
      
    }
    
    public Proveedor buscarProveedor(int id){
        
        return pdao.buscarProveedor(id);
    }
    
    public List<Producto> listarProducto(){
       return prdao.listaProductos();
    }
    
     public List<Proveedor> listarProveedores(){
       return pdao.listaProveedor();
    }
    
}
