/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenferreteriajee.services;

import ec.edu.ups.examenferreteriajee.modelo.Producto;
import ec.edu.ups.examenferreteriajee.modelos.Proveedor;
import ec.edu.ups.examenferreteriajee.on.ObjetoNegociosLocal;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 *
 * @author Luis Mario
 */


public class WebServices {
    
   private static String ws_solicitar="http://localhost:8081/ExamenProveedor1JEE/rest/proveedor/solicitar";
   
   private static String ws_buscar="http://localhost:8081/ExamenProveedor1JEE/rest/proveedor/buscar";
   
   
   public static Producto buscarProducto(int id){
       Client cliente= ClientBuilder.newClient();
       WebTarget target = cliente.target(ws_buscar).queryParam("id", id);
       
       Producto p = target.request().get(Producto.class);
       cliente.close();
       return p;
   }
   
   
   public static boolean solicitarProducto(int id, int cantidad){
       
        Client cliente= ClientBuilder.newClient();
       WebTarget target = cliente.target(ws_solicitar).queryParam("id", id).queryParam("cantidad", cantidad);
       
       
       return target.request().get(Boolean.class);
   }
}
