/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenferreteriajee.bean;

import ec.edu.ups.examenferreteriajee.modelo.Producto;
import ec.edu.ups.examenferreteriajee.modelos.Proveedor;
//import ec.edu.ups.examenferreteriajee.modelos.ServiciosWeb_ServiciosWebPort_Client;
import ec.edu.ups.examenferreteriajee.on.ObjetoNegociosLocal;
import ec.edu.ups.examenferreteriajee.services.WebServices;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

/**
 *
 * @author Luis Mario
 */

@ManagedBean
@ViewScoped
public class InsertarProducto {
    
    @PostConstruct
    public void init(){
        producto = new Producto();
        proveedores= ob.listarProveedores();
    }
    @Inject
    private ObjetoNegociosLocal ob;
    
    private Producto producto;
    
    
    private List<Proveedor> proveedores;
   

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public InsertarProducto() {
        
    }
    
    public String guardarProducto(){
        
        try {
            
            
            
       Producto producto_aux = WebServices.buscarProducto(producto.getId());   
            Proveedor proveedor_aux = proveedores.get(Integer.valueOf(proveedor)-1);
           
            
            
            if(producto_aux!=null){
                
                producto_aux.setStock(producto.getStock());
                ob.insertarP(proveedor_aux, producto_aux);
                WebServices.solicitarProducto(producto.getId(), producto.getStock());
                System.out.println("Producto ingresado....");
                
                
            }else
            {
                System.out.println("No se ha encontrado el producto");
            }
            
            
        } catch (Exception ex) {
            Logger.getLogger(InsertarProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    private String proveedor;

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public SelectItem[] getProveedores() {
       SelectItem[] datos= new SelectItem[2];
        datos[0]= new SelectItem(1,"guapan");
         datos[1]= new SelectItem(2,"holsim");
        return datos;
    }
    
    
    
    
    
}
