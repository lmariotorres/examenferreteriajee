/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenferreteriajee.dao;

import ec.edu.ups.examenferreteriajee.modelo.Producto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Luis Mario
 */

@Stateless
public class ProductoDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public void insertarProducto (Producto producto){
        em.persist(producto);
    }
    
    public Producto read (int codigo){
        return em.find(Producto.class, codigo);
    }
    
    public void actualizar (Producto producto){
        em.merge(producto);
        
    }
    
  public List<Producto> listaProductos (){
        
        Query q = em.createQuery("Select p from Producto p",Producto.class);
        return q.getResultList();
    }
    
}
