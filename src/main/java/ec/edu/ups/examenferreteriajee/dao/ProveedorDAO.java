/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenferreteriajee.dao;

import ec.edu.ups.examenferreteriajee.modelos.Proveedor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class ProveedorDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    
    public void guardarProveedor (Proveedor proveedor){
        
        em.persist(proveedor);   
    }
  
    public void actualizarProveedor (Proveedor proveedor){
        em.merge(proveedor);
    }
    
    public List<Proveedor> listaProveedor (){
        
        Query q = em.createQuery("Select p from Proveedor p",Proveedor.class);
        return q.getResultList();
    }
    public Proveedor buscarProveedor (int id){
       return em.find(Proveedor.class, id);
       
    }
    
    
}
