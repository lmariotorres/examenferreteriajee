/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenferreteriajee.on;

import ec.edu.ups.examenferreteriajee.modelo.Producto;
import ec.edu.ups.examenferreteriajee.modelos.Proveedor;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Luis Mario
 */

@Local
public interface ObjetoNegociosLocal {
    
   public void insertarP(Proveedor proveedor, Producto producto) throws Exception;
    
    
    public void solicitarP (int cantidad, Producto producto) throws Exception;
    
     public Proveedor buscarProveedor(int id);
      public List<Producto> listarProducto();
      
      public List<Proveedor> listarProveedores();
}
