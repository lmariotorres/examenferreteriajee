/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.ups.examenferreteriajee.bean;

import ec.edu.ups.examenferreteriajee.modelo.Producto;
import ec.edu.ups.examenferreteriajee.on.ObjetoNegociosLocal;
import ec.edu.ups.examenferreteriajee.services.WebServices;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Luis Mario
 */

@ManagedBean
@ApplicationScoped
public class ProductosBean {
    
    @Inject
    private ObjetoNegociosLocal ob;
    
    private List<Producto> productos;
    
    
    
    private Producto producto;

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

   
 
    private int cantidad;

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
    
    public ProductosBean(){
        
    }
    
    @PostConstruct
    public void init(){
        productos = ob.listarProducto();
        
       
    }
    
    public String Solicitar(Producto producto){
        
       this.producto=producto;
        return  "solicitar.xhtml?faces-redirect=true";
    }
    
    
    public String guardar(){
         try {
            
            
          
         boolean respuesta = WebServices.solicitarProducto(producto.getId(), cantidad);
         
         if(respuesta){
             ob.solicitarP(cantidad, producto);
             productos=ob.listarProducto();
                System.err.println("Se actualizo el inventario");
          }else{
             System.err.println("No se actualizo!!!!!");
         }
           
          
           
           
        } catch (Exception ex) {
            Logger.getLogger(ProductosBean.class.getName()).log(Level.SEVERE, null, ex);
            
            
             
        }
         
         return "listado";
    }
    
  
    
}
